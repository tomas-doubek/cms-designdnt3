<?php
function fonts(){
	return array(
		"roboto" 				=> "Roboto",
		"Arial" 				=> "Arial",
		"Georgia" 				=> "Georgia",
		"Calibri" 				=> "Calibri",
		"Cambria" 				=> "Cambria",
		"Lucida Sans Unicode" 	=> "Lucida Sans Unicode",
		"Myriad Pro Regular " 	=> "Myriad Pro Regular",
		"Tahoma" 				=> "Tahoma",
		"Verdana" 				=> "Verdana",
		//"Times New Roman" 	=> "Times New Roman",
		//"bebas_neueregular" 	=> "Bebas",
		//"impact" 	=> "Impact",
	);
}